const canvas = document.getElementById('gameCanvas');
const context = canvas.getContext('2d');

let snake = [{ top: 200, left: 200 }];
let direction = 'right';

function drawSnakePart(snakePart) {
    context.fillStyle = 'lightgreen';
    context.strokeStyle = 'darkgreen';
    context.fillRect(snakePart.top, snakePart.left, 20, 20);
    context.strokeRect(snakePart.top, snakePart.left, 20, 20);
}

function drawSnake() {
    snake.forEach(drawSnakePart);
}

function updateSnake() {
    const head = { ...snake[0] }; 
    switch (direction) {
        case 'right':
            head.left += 20;
            break;
        case 'down':
            head.top += 20;
            break;
        case 'left':
            head.left -= 20;
            break;
        case 'up':
            head.top -= 20;
            break;
    }
    snake.unshift(head);
    snake.pop();
}

function clearCanvas() {
    context.fillStyle = 'white';
    context.strokeStyle = 'black';
    context.fillRect(0, 0, canvas.width, canvas.height);
    context.strokeRect(0, 0, canvas.width, canvas.height);
}

function main() {
    setTimeout(function onTick() {
        clearCanvas();
        drawSnake();
        updateSnake();
        main();
    }, 100);
}

main();
